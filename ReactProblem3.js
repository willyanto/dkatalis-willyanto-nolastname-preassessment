const Product = props => {
  const plus = () => {
    // Call props.onVote to increase the vote count for this product
    // change code below this line
    props.onVote(1, props.index);
    // change code above this line
  };
  const minus = () => {
    // Call props.onVote to decrease the vote count for this product
    // change code below this line
    props.onVote(-1, props.index);
    // change code above this line
  };
  return (
      <li>
        <span>{ props.item.name }</span> - <span>votes: { props.item.votes }</span>
        <button onClick={plus}>+</button>{" "}
        <button onClick={minus}>-</button>
      </li>
  );
};







class GroceryApp extends React.Component {

  // Finish writing the GroceryApp class
  constructor(props) {
    super(props);
    this.state = {
      products: props.products,
    };
  }

  onVote = (dir, index) => {
    // Update the products array accordingly â€¦
    // change code below this line
    var products = this.state.products;
    var product = products[index];
    product.votes = product.votes + dir;
    products.splice(index, 1, product);
    this.setState({products: products});
    // change code above this line
  };

  render() {
    return (
        <ul>
          {/* Render an array of products, which should call this.onVote when + or - is clicked */}
          {/*  change code below this line */}
          { this.state.products.map((product, index) => <Product item={product} onVote={this.onVote} index={index} /> ) }
          {/*  change code above this line */}
        </ul>
    );
  }
}

document.body.innerHTML = "<div id='root'></div>";

ReactDOM.render(<GroceryApp
    products={[
      { name: "Oranges", votes: 0 },
      { name: "Apples", votes: 0 },
      { name: "Bananas", votes: 0 }
    ]}
/>, document.getElementById('root'))
